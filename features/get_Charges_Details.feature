@get_Charges_Detail
Feature: Charges
	As an API consumer
	I want to query charge requests
	So that I know they have been processed

	@get-Charges-Details_byRefNumber
	Scenario Outline: Verify the API Get charges with RefNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TransactionType>
		Then response code should be 201
	#	When I use HMAC and GET /charges?type=<TransactionType>
		And I get that same transaction details using reference number
		Then response code should be 200
		And response body should contain the same reference number
		And response header Content-Type should be application/json
		And response body path should contain "type":"<TransactionType>"
	 #	And response body path $.items.[transactionCode] should be <TransactionType>
		And response body path should contain "service":"Bankcard"

		Examples:
			|TransactionType  |
			|Sale			  |
			|Authorization  |
			|Force 		  |
		  # |Void 		  |
			|Credit 		  |
		 # |CreditByReference|
         # |Capture		  |

	
	############### Negative Scenarios ###################


	@get-Charges-Details_byRefNumber
	Scenario: Verify the API Get charges with RefNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /charges/Test123
		Then response code should be 404
		And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.
		
		
		
		
		
		