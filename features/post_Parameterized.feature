@charges_POST_Parameterized
Feature: Charges
	As an API consumer
	I want to create charge requests
	So that I know they can be processed

	@post-Sale_Auth_Force_ForAllCard_SandBox
    Scenario Outline: Post a Sale,Auth and force transaction using Visa,Mastercard,Amex,Discover cards for different amount, different expiration year.
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to <BodyValue>
		When I use HMAC and POST to /charges?type=<TypeofTx>
        Then response code should be 201
     #   And response body should be valid json
		And response header Content-Type should be application/json
		And response body path status should be Approved
	
		Examples:
		|	CardName	|	BodyValue																											                                            | TypeofTx    |
		|	Visa		|	{"retail": {"amounts": {"total": 15},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "4111111111111111","expiration": "0618"}}}	|Sale         |
		|	MasterCard  |	{"retail": {"amounts": {"total": 22},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "5499740000000057","expiration": "0618"}}}	|Sale         |
		|	Amex		|	{"retail": {"amounts": {"total": 17},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "6011000993026909","expiration": "0618"}}}	|Sale         |
		|	Discover	|	{"retail": {"amounts": {"total": 18},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "371449635392376","expiration": "0618"}}}	    |Sale         |
		|	Visa		|	{"retail": {"amounts": {"total": 19},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "4111111111111111","expiration": "0618"}}}	|Authorization|
		|	MasterCard  |	{"retail": {"amounts": {"total": 20},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "5499740000000057","expiration": "0618"}}}	|Authorization|
		|	Amex		|	{"retail": {"amounts": {"total": 21},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "6011000993026909","expiration": "0618"}}}	|Authorization|
		|	Discover	|	{"retail": {"amounts": {"total": 22},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "371449635392376","expiration": "0618"}}}	    |Authorization|
		|	Visa		|	{"retail": {"amounts": {"total": 23},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "4111111111111111","expiration": "0618"}}}    |Force		  |
		|	MasterCard  |	{"retail": {"amounts": {"total": 24},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "5499740000000057","expiration": "0618"}}}    |Force		  |
		|	Amex		|	{"retail": {"amounts": {"total": 6.2},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "6011000993026909","expiration": "0618"}}}    |Force        |
		|	Discover	|	{"retail": {"amounts": {"total": 6.3},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "371449635392376","expiration": "0618"}}}     |Force        |		
		|	Visa		|	{"ECommerce": {"amounts": {"total": 6.4},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "4111111111111111","expiration": "0618"}}}	|Authorization|		
		|	MasterCard  |	{"ECommerce": {"amounts": {"total": 6.5},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "5499740000000057","expiration": "0618"}}}	|Authorization|
		|	Amex		|	{"ECommerce": {"amounts": {"total": 6.6},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "6011000993026909","expiration": "0618"}}}	|Authorization|
		|	Discover	|	{"ECommerce": {"amounts": {"total": 6.7},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "371449635392376","expiration": "0618"}}}	|Authorization|
		|	Visa		|	{"ECommerce": {"amounts": {"total": 6.8},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "4111111111111111","expiration": "0618"}}}	|Force        |
		|	MasterCard  |	{"ECommerce": {"amounts": {"total": 6.9},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "5499740000000057","expiration": "0618"}}}	|Force        |
		|	Amex		|	{"ECommerce": {"amounts": {"total": 7.1},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "6011000993026909","expiration": "0618"}}}	|Force        |
		|	Discover	|	{"ECommerce": {"amounts": {"total": 7.2},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "371449635392376","expiration": "0618"}}}	|Force        |		
		|	Visa		|	{"ECommerce": {"amounts": {"total": 7.3},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "4111111111111111","expiration": "0618"}}}	|Sale         |		
		|	MasterCard  |	{"ECommerce": {"amounts": {"total": 7.4},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "5499740000000057","expiration": "0618"}}}	|Sale         |
		|	Amex		|	{"ECommerce": {"amounts": {"total": 7.5},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "6011000993026909","expiration": "0618"}}}	|Sale         |
		|	Discover	|	{"ECommerce": {"amounts": {"total": 7.8},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "371449635392376","expiration": "0618"}}}	|Sale         |
		|	Visa		|	{"HealthCare": {"amounts": {"total": 7.9},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "4111111111111111","expiration": "0618"}}}	|Force        |
		|	MasterCard  |	{"HealthCare": {"amounts": {"total": 8.1},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "5499740000000057","expiration": "0618"}}}	|Force        |
		|	Amex		|	{"HealthCare": {"amounts": {"total": 8.2},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "6011000993026909","expiration": "0618"}}}    |Force        |
		|	Discover	|	{"HealthCare": {"amounts": {"total": 8.3},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "371449635392376","expiration": "0618"}}}	    |Force        |		
		|	Visa		|	{"HealthCare": {"amounts": {"total": 8.4},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "4111111111111111","expiration": "0618"}}}	|Sale         |		
		|	MasterCard  |	{"HealthCare": {"amounts": {"total": 8.5},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "5499740000000057","expiration": "0618"}}}	|Sale         |
		|	Amex		|	{"HealthCare": {"amounts": {"total": 8.6},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "6011000993026909","expiration": "0618"}}}	|Sale         |
		|	Discover	|	{"HealthCare": {"amounts": {"total": 8.7},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "371449635392376","expiration": "0618"}}}	    |Sale         |
		|	Visa		|	{"HealthCare": {"amounts": {"total": 8.8},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "4111111111111111","expiration": "0618"}}}	|Authorization|
		|	MasterCard  |	{"HealthCare": {"amounts": {"total": 8.9},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "5499740000000057","expiration": "0618"}}}	|Authorization|
		|	Amex		|	{"HealthCare": {"amounts": {"total": 9.1},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "6011000993026909","expiration": "0618"}}}	|Authorization|
		|	Discover	|	{"HealthCare": {"amounts": {"total": 9.2},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "371449635392376","expiration": "0618"}}}	    |Authorization|
		# Below scanario is for level 3 transactions
		#|	Visa		|	{"ECommerce":{"Amounts":{"Total":50.0},"orderNumber":"'+ timestamp + '","CardData":{"Expiration":"2516","Number":"4111111111111111"}},"level3": {"destinationCountryCode": "US","amounts": {"discount": 2, "duty": 1, "nationalTax": 1 },"vat": {"idNumber": "55555","invoiceNumber": "44444","amount": 10,"rate": 4},"customerNumber": "666666"}} 	|sale         |
		#|	MasterCard  |	{"ECommerce":{"Amounts":{"Total":51.0},"orderNumber":"'+ timestamp + '","CardData":{"Expiration":"2516","Number":"4111111111111111"}},"level3": {"destinationCountryCode": "US","amounts": {"discount": 2, "duty": 1, "nationalTax": 1 },"vat": {"idNumber": "55555","invoiceNumber": "44445","amount": 10,"rate": 4},"customerNumber": "666666"}} 	|sale         |
		#|	Amex		|	{"ECommerce":{"Amounts":{"Total":52.0},"orderNumber":"'+ timestamp + '","CardData":{"Expiration":"2516","Number":"4111111111111111"}},"level3": {"destinationCountryCode": "US","amounts": {"discount": 2, "duty": 1, "nationalTax": 1 },"vat": {"idNumber": "55555","invoiceNumber": "44446","amount": 10,"rate": 4},"customerNumber": "666666"}} 	|sale         |
		#|	Discover	|	{"ECommerce":{"Amounts":{"Total":53.0},"orderNumber":"'+ timestamp + '","CardData":{"Expiration":"2516","Number":"4111111111111111"}},"level3": {"destinationCountryCode": "US","amounts": {"discount": 2, "duty": 1, "nationalTax": 1 },"vat": {"idNumber": "55555","invoiceNumber": "44447","amount": 10,"rate": 4},"customerNumber": "666666"}} 	|sale         |
		# Below scanario is for Recurring transactions
		#|	Visa		|	{"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}} 		|sale          	|
		#|	MasterCard		|	{"transactionId": "tid-1234","ECommerce": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"5499740000000057","cvv":"123"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}} 	|Authorization  |
		#|	Amex		|	{"transactionId": "tid-1234","HealthCare": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"371449635392376","cvv":"123"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}} 	|force 			|
		
		