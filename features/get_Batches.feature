@get_Batches
Feature: Get Batches
	As an API consumer
	I want to query batches requests
	So that I know they have been processed

	@get_Batches_AllBatchTransactions
    Scenario: query to Get all Batch API Transactions
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Sale
		Then response code should be 201
	#	And I set body to { "settlementType": "Bankcard",     "count": 10,     "net": 10,     "terminalNumber": "" }
		And I set body to { "settlementType": "Bankcard"}
		When I use HMAC and POST to /batches/current
	#	When I use HMAC and settle all current set of transactions using /batches/current
		Then response code should be 201
		And response body path status should be Approved
		And response body path message should be BATCH 1 OF 1 CLOSED
		And I use HMAC then GET List of Existing Settled Batches Transactions using /batches
        Then response code should be 200
		And response body should contain the same reference number
		And response header Content-Type should be application/json
		And response body path should contain "reference"
		And response body path should contain "service"
		And response body path should contain "terminalNumber"
		And response body path should contain "count"
		And response body path should contain "volume"

		
	@get-Batches-byStartDate
    Scenario: Verify Get Batches API with StartDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /batches?startDate=2017-08-13
        Then response code should be 200
        And response body path should contain "startDate":"2017-08-13"
		And response body path should contain "reference"
		And response body path should contain "service"
		And response body path should contain "terminalNumber"
		And response body path should contain "date"
		And response body path should contain "count"
		And response body path should contain "net"
		And response body path should contain "volume"
		
	@get-Batches-byEndDate
    Scenario: Verify Get Batches API with EndDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /batches?endDate=2017-11-10
        Then response code should be 200
        And response body path should contain "endDate":"2017-11-10"
		And response body path should contain "reference"
		And response body path should contain "service"
		And response body path should contain "terminalNumber"
		And response body path should contain "date"
		And response body path should contain "count"
		And response body path should contain "net"
		And response body path should contain "volume"
		
	@get-Batches-byPageSize
    Scenario: Verify the Get Batches API with PageSize and transaction Items count should match
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /batches?PageSize=2
        Then response code should be 200
        And response body path should contain "pageSize":2
		And response body path should contain "reference"
		And response body path should contain "service"
		And response body path should contain "terminalNumber"
		And response body path should contain "date"
		And response body path should contain "count"
		And response body path should contain "net"
		And response body path should contain "volume"
		And response body path should contain "service":"Bankcard"
		
			
	@get-Batches-byPageNumber
    Scenario: Verify the Get batches API with PageNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /batches?pageNumber=1
        Then response code should be 200
        And response body path should contain "pageNumber":1
		And response body path should contain "reference"
		And response body path should contain "service"
		And response body path should contain "terminalNumber"
		And response body path should contain "date"
		And response body path should contain "count"
		And response body path should contain "net"
		And response body path should contain "volume"
		
		
		# +++++++++++++++++    Negative Scenarios   ++++++++++++++++++++++++++
	 
		
	@get-Batches-StartDate_AsFutureDate
     Scenario: Verify the API Get batches with Future StartDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /batches?StartDate=2030-12-12
        Then response code should be 200
        And response body path should contain "startDate":"2030-12-12"
		And response body path should not contain "reference"
		And response body path should not contain "service"
		And response body path should not contain "terminalNumber"
		And response body path should not contain "date"
		And response body path should not contain "count"
		And response body path should not contain "net"
		And response body path should not contain "volume"
		
	@get-Batches-EndDate_AsPastDate
     Scenario: Verify the API Get batches with Past EndDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /batches?EndDate=2012-12-12
        Then response code should be 200
        And response body path should contain "endDate":"2012-12-12"
		And response body path should not contain "reference"
		And response body path should not contain "service"
		And response body path should not contain "terminalNumber"
		And response body path should not contain "date"
		And response body path should not contain "count"
		And response body path should not contain "net"
		And response body path should not contain "volume"
		
#	Can't Create Negative Scenario for Page Number

#	@get-Batches-byinValidPageNumber
#     Scenario: Verify the API Get batches with inValidPageNumber
#      Given I set clientId header to `clientId`
#		And I set merchantId header to `merchantId`
#		And I set merchantKey header to `merchantKey`
#		And I set Content-Type header to "application/json"
#		When I use HMAC and GET /batches?PageNumber=ABC
#        Then response code should be 200
#       And response body should not contain "pageNumber":ABC	  

#	Can't Create Negative Scenario for PageSize	
	  
#	 @get-Batches-byinValidPageSize
#     Scenario: Verify the API Get batches with inValidPageSize
#       Given I set clientId header to `clientId`
#		And I set merchantId header to `merchantId`
#		And I set merchantKey header to `merchantKey`
#		And I set Content-Type header to "application/json"
#		When I use HMAC and GET /batches?PageSize=FSFSDFSDFSFF
#        Then response code should be 200
#        And response body should not contain "pageSize":FSFSDFSDFSFF	 
	
	
	
   