@get_Credits
Feature: Get Credits
	As an API consumer
	I want to query credits requests
	So that I know they have been processed

  @get_Credits_AllCreditTransactions
  Scenario: query for get all Credit transactions
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 10.24,             "total": 31.44,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "vinayak@foo.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    And I use HMAC and then GET a list of existing Credit transaction
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "service":"Bankcard"
    And response body path should contain "type":"Credit"

  @get-Credits_byStartDate
  Scenario: Verify the API Get credits with StartDate
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?startDate=2017-08-17
    Then response code should be 200
    And response body path should contain "startDate":"2017-08-17"
    And response header Content-Type should be application/json

  @get-Credits_byEndDate
  Scenario: Verify the API Get credits with EndDate
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?endDate=2017-10-10
    Then response code should be 200
    And response body path should contain "endDate":"2017-10-10"
    And response header Content-Type should be application/json

  @get-Credits_byPageSize
  Scenario: Verify the API Get credits with PageSize
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?PageSize=1
    Then response code should be 200
    And response body path should contain "pageSize":1
    And response header Content-Type should be application/json

  @get-Credits_byPageNumber
  Scenario: Verify the API Get credits with PageNumber
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?pageNumber=2
    Then response code should be 200
    And response body path should contain "pageNumber":2
    And response header Content-Type should be application/json

  @get-Credits_byIsPurchaseCard
  Scenario Outline: Verify the API Get credits with IsPurchaseCard
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?IsPurchaseCard=<IsPurchaseCard>
    Then response code should be 200
    And response body path should contain "isPurchaseCard":<IsPurchaseCard>
    And response header Content-Type should be application/json

    Examples:
      |IsPurchaseCard    |
     #	|true    		   |
      |false             |

  @get-Credits_byGatewayID
  Scenario: Verify the API Get credits with GatewayID
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?GatewayId=999999999997
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "service":"Bankcard"
    And response body path should contain "gatewayId":"999999999997"

  @get_Credits_byValidReferenceNumber
  Scenario: query for get credit transaction detials by reference number
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 10.24,             "total": 39.57,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "vinayak@foo.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    And I use HMAC then GET same credit transaction details using /credits?reference=
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "service":"Bankcard"
    And response body path should contain "type":"Credit"

  @get-Credits_byOrderNumber
  Scenario: Verify the API Get credits with Bankcard OrderNumber
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 5.24,             "total": 60.15,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "vinayak@foo.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    When I use HMAC and GET /credits?OrderNumber=7687677
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "service":"Bankcard"
    And response body path should contain "orderNumber":"7687677"

  @get-Credits_byTotalAmount
  Scenario: Verify the API Get credits with TotalAmount
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 5.24,             "total": 97,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "vinayak@foo.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    When I use HMAC and GET /credits?total=99
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "service":"Bankcard"
    And response body path should contain "total":97

  @get-Credits_byApprovalCode
  Scenario: Verify the API Get credits with Bankcard ApprovalCode
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 5.24,             "total": 12.88,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "vinayak@foo.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
    When I use HMAC and POST to /credits
    Then response code should be 201
    When I use HMAC and GET /credits?approvalCode=213213
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "service":"Bankcard"
    And response body path should contain "responseCode":"213213"

  @get-Credits_byStatus
  Scenario Outline: Verify the API Get credits with Status
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?Status=<Status>
    Then response code should be 200
    And response body path should contain "status":"<Status>"

    Examples:
      |Status  |
      |Declined|
      |Batch   |
      |Settled |
 #	|Expired |

  @get-Credits_byAccountNumber
  Scenario Outline: Verify the API Get credits with AccountNumber
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?accountNumber=<number>
    Then response code should be 200
    And response header Content-Type should be application/json
    And response body path should contain "accountNumber":"<accountNumber>"
    Examples:
      | number | accountNumber |
      | 1111   | XXXXXXXXXXXX1111 |
      | 5454   | XXXXXXXXXXXX5454 |
      | 6909   | XXXXXXXXXXXX6909 |
      | 2376   | XXXXXXXXXXX2376  |


  # +++++++++++++++++    Negative Scinarios   ++++++++++++++++++++++++++

  @get-Credits_byinValidReference
  Scenario: Verify the API Get credits with inValidReference
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?reference=ABCABC1234
    Then response code should be 200
    And response body path should not contain "reference":"ABCABC1234"

  @get-Credits_StartDate_AsFutureDate
  Scenario: Verify the API Get Credits with Future StartDate
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?StartDate=2030-12-12
    Then response code should be 200
    And response body path should contain "startDate":"2030-12-12"
    And response body path should not contain "reference"
    And response body path should not contain "service"
    And response body path should not contain "paymentType"
    And response body path should not contain "saleCount"
    And response body path should not contain "saleTotal"
    And response body path should not contain "creditCount"
    And response body path should not contain "creditTotal"
    And response body path should not contain "totalCount"
    And response body path should not contain "totalVolume"

  @get-Credits_EndDate_AsPastDate
  Scenario: Verify the API Get Credits with Past EndDate
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?EndDate=2012-12-12
    Then response code should be 200
    And response body path should contain "endDate":"2012-12-12"
    And response body path should not contain "reference"
    And response body path should not contain "service"
    And response body path should not contain "paymentType"
    And response body path should not contain "saleCount"
    And response body path should not contain "saleTotal"
    And response body path should not contain "creditCount"
    And response body path should not contain "creditTotal"
    And response body path should not contain "totalCount"
    And response body path should not contain "totalVolume"

  @get-Credits_byinValidGatewayID
  Scenario: Verify the API Get credits with inValidGatewayId
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?GatewayID=FSFSDFSDFSFF
    Then response code should be 200
    And response body path should not contain "gatewayId":"FSFSDFSDFSFF"

  @get-Credits_byinValidOrderNumber
  Scenario: Verify the API Get credits with inValidOrderNumber
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?orderNumber=FSFSDFSDFSFF
    Then response code should be 200
    And response body path should not contain "orderNumber":"`FSFSDFSDFSFF`"

  @get-Credits_byinValidApprovalCode
  Scenario: Verify the API Get credits with inValidApprovalCode
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?approvalCode=FSFSDFSDFSFF
    Then response code should be 200
    And response body path should not contain "responseCode":"`FSFSDFSDFSFF`"

  @get-Credits_byinValidTransactionCode
  Scenario: Verify the API Get credits with inValidTransactionCode
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?TransactionCode=FSFSDFSDFSFF
    Then response code should be 200
    And response body path should not contain "transactionCode":"FSFSDFSDFSFF"

  @get-Credits_byinValidStatus
  Scenario: Verify the API Get credits with inValidStatus
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?Status=fkjsdklfdsjsfdf
    Then response code should be 200
    And response body path should not contain "status":"fkjsdklfdsjsfdf"

  @get-Credits_byinValidSource
  Scenario: Verify the API Get credits with inValidSource
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?Source=FSDFDSFDSFSFSSF
    Then response code should be 200
    And response body path should not contain "source":"FSDFSDSFFSDFS"

  @get-Credits_byinValidIsPurchaseCard
  Scenario: Verify the API Get credits with inValidPurchaseCard
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    When I use HMAC and GET /credits?IsPurchaseCard=FSDFDSFDSFSFSSF
    Then response code should be 200
    And response body path should not contain "source":"FSDFSDSFFSDFS"


