@Post_Batches_Current
Feature: Post_Batches_Current
	As an API consumer
	I want to settle the current set of transaction
	So that I know this flow works correctly

	@batch-Settle_bySaleForce
	Scenario Outline: Perform a transaction and then settle the current set of transactions
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
		When I use HMAC and settle all current set of transactions using /batches/current
		Then response code should be 201
		And response body path status should be Approved
		And response body path message should be BATCH 1 OF 1 CLOSED

		Examples:
			|	TypeOfTransaction	|
			|	Sale				|
			|	Force				|

	@batch-Settle_byAuthorization
	Scenario: Perform a transaction and then settle the current set of transactions
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Authorization
		Then response code should be 201
		And I Capture the Authorization transaction with Total Amount 11
		Then response code should be 200
		When I use HMAC and settle all current set of transactions using /batches/current
		Then response code should be 201
		And response body path status should be Approved
		And response body path message should be BATCH 1 OF 1 CLOSED

	@batch-Settle_byCredit
	Scenario: Perform a transaction and then settle the current set of transactions
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /credits
		Then response code should be 201
		When I use HMAC and settle all current set of transactions using /batches/current
		Then response code should be 201
		And response body path status should be Approved
		And response body path message should be BATCH 1 OF 1 CLOSED

		# *******************  Negative Test Cases  *************************************
		
	@batch-settleNoTransaction
	Scenario: Try to Settle when there is no open transaction
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and settle all current set of transactions using /batches/current
		Then response code should be 200
		And response body path message should be NO TRANSACTIONS

	@batch-Settle_byAuthorization_WithoutCapture
	Scenario: Perform a Auth transaction and then tyr to settle by without Capture it
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Authorization
		Then response code should be 201
		When I use HMAC and settle all current set of transactions using /batches/current
		Then response code should be 200
		And response body path should not contain Approved
		And response body path should not contain BATCH 1 OF 1 CLOSED
		And response body path message should be NO TRANSACTIONS
