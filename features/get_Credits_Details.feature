@get_Credits_Detail
Feature: Get Credits Detail
	As an API consumer
	I want to query charge requests
	So that I know they have been processed

	@get_Credits_Details-With_ValidReferenceNumber
	Scenario: Verify the API get_Credits_Detail with valid ReferenceNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 10.24,             "total": 13.10,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "vinayak@foo.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
		When I use HMAC and POST to /credits
		Then response code should be 201
		And I use HMAC then GET same credit transaction details using /credits/
		Then response code should be 200
		And response body should contain the same reference number
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		And response body path should contain "type":"Credit"

	
#	**************** Negative Scenarios **************************
	
	@get_Credits_Detail_byInvalidReferenceNumber
    Scenario: Verify the API Get_Credits Details with Invalid Reference Number
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /credits/Test123
		Then response code should be 404
		And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.
	  

		
		
		
		
		
		
		
		