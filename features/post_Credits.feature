@credits_POST
Feature: Post Credits
	As an API consumer
	I want to create charge requests
	So that I know they can be processed

	@post_Credits-byCreditReference
	Scenario: Verify Post Credit by Valid Json body
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 5.24,             "total": 42.52,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "vinayak@foo.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
	#	And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /credits
		Then response code should be 201
		And response header Content-Type should be application/json
		And response body path status should be Approved
		And response body path message should be APPROVED
		And I get that same transaction details
		Then response code should be 200

	@post-Credits-WithParameterizeData
	Scenario Outline: API Post Charge Lineitems with Valid Reference Number and Lineitems Body
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "",     "<Type>": {         "amounts": {             "tip": 4.24,             "total": <Amount>,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "<OrderNumber>",         "cardData": {             "number": "<CardNumber>",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "<Email>",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": <IsRecurring>      } }
		When I use HMAC and POST to /credits
		Then response code should be 201
		And response header Content-Type should be application/json
		And response body path status should be Approved
		And response body path message should be APPROVED
		
		Examples:
		|Type       |CardNumber 	   |Amount  |OrderNumber | CustomerName  | Email 			|IsRecurring |
		|retail     |4111111111111111  | 51.13  |1234567     | San Kumar     |vinayak@foo.com   |false |
		|eCommerce  |5499740000000057  | 61.13  |1234568 	 | Midhun Kumar  |midhun@test.com   |true  |
		|healthcare |6011000993026909  | 71.13  |1234569 	 | Sateesh Kumar |sateesh@gmail.com |false |
		|eCommerce  |371449635392376   | 81.13  |1234560 	 | Bharath Kumar |bharath@yahoo.com |true  |
		
		
		# **************  Negative Scenarios  *************************


	@post-Credits_byInvalidAmount
	Scenario: Post a credit for invalid Amount
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234", "ECommerce": { "Amounts": { "Total": -48.0 },"authorizationCode": "123456","CardData": { "Expiration": "1216", "Number": "4111111111111111" } } }
		When I use HMAC and POST to /credits
		Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.Amounts: Invalid Total Amount
		
	@post-Credits_byInvalidCardNumber
    Scenario: Post a credit for invalid CardNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "1216", "Number": "4111111111111111234" } } }		
		When I use HMAC and POST to /credits
        Then response code should be 401

		And response body path code should be 100008
		And response body path message should be Invalid message request content
		And response body path detail should be You are using certification environment which is restricted to process transaction using test Credit cards only.

	 #	And response body path code should be 400000
	 #	And response body path message should be There was a problem with the request. Please see 'detail' for more.
	 #	And response body path detail should be InvalidRequestData : INVALID C_CARDNUMBER

	@post-Credits_byInvalidCVV
    Scenario: Post a credit for invalid CVV
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "1216", "Number": "4111111111111111", "cvv": "@#$%*@#$*" } } }		
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The field CVV must be a string or array type with a maximum length of '4'.
		
	@post-Credits_byCVVMoreThan4Digits
    Scenario: Post a credit for CVV value More Than 4 Digits
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "1216", "Number": "4111111111111111", "cvv": "12345" } } }		
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The field CVV must be a string or array type with a maximum length of '4'.
     
	@post-Credits_byInvalidEmailId
    Scenario: Post a credit for invalid EmailId
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 4.24,             "total": 42.42,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "sankumar.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.Retail.Customer: The Email field is not a valid e-mail address.
		
	@post-Credits_byBlankEmailId
    Scenario: Post a credit for Blank EmailId
        Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 4.24,             "total": 42.42,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.Retail.Customer: The Email field is not a valid e-mail address.
		
	@post-Credits_byInvalidExpDate
    Scenario: Post a credit for invalid Expiry Date
        Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "20-12", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.
		
	@post-Credits-Credit_byExpDateLessThan4Digits
    Scenario: Post a credit by Expire Date Less Than 4 Digits
        Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "20", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.
		
	@post-Credits-Credit_byExpDateGreaterThan4Digits
    Scenario: Post a credit by Expire Date Greater Than 4 Digits
        Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "20255", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.
		
	@post-Credits_byBlankExpireDate
    Scenario: Post a credit for Blank Expiry Date
        Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The Expiration field is required.; The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.
		
	@post-Credits_ExpireDateWithNonDigits
    Scenario: Post a credit forExpiry Date with Non Digits
        Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "abcd", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /credits
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: Expiration must be a number
	
