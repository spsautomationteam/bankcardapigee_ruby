@Delete_Charges_LineItems
Feature: Delete_Charges_LineItems
	As an API consumer
	I want to create charge requests
	So that I know they can be processed

	@delete_Charges_Lineitems_Details_byAll_TransactionTypes
	Scenario Outline: API Get Charge Lineitems with Valid Reference Number and Lineitems Body
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
		When I use HMAC and POST LineItems using lineItem Body {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
		Then response code should be 201
		And I use HMAC and DELETE lineitems to /charges/{reference}/lineitems
		Then response code should be 204

		Examples:
			|	TypeOfTransaction	|
			|	Sale				|
			|	Authorization		|
			|	Force				|
		#	|   Credit 				|


		# **************  Negative Scenarios  *************************
		
	@delete_Charges_LineitemsDetails_byWithOut_PostLineItem_Sale
	Scenario Outline: API Get Charge Lineitems with Valid Reference Number and WithOut PostLineItem
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
		And I use HMAC and DELETE lineitems to /charges/{reference}/lineitems
		Then response code should be 204

		Examples:
		|	TypeOfTransaction	|
		|	Sale				|
		|	Authorization		|
		|	Force				|
	  #	|   Credit 				|

	@delete_Charges_Lineitems_Details_byInValidRefNumber
	Scenario: API Get Charge Lineitems with InValid Reference Number and Lineitems Body
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        And I use HMAC and DELETE lineitems to /charges/Test123/lineitems
		Then response code should be 404
		And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.


