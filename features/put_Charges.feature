@put_Charges
Feature: Authorization and Capture
	As an API consumer
	I want to create an authorization charge and then capture
	So that I know this flow works correctly

	@put_Charges-byAuthTransaction
	Scenario: Post an Authorization Transaction and Capture the Same transaction
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":40},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Authorization
		Then response code should be 201
		And I Capture the Authorization transaction with Total Amount 11
		Then response code should be 200
		And I get that same transaction details by using reference number and /charges/
		Then response code should be 200
		And response body path amounts.total should be 11.0
		
	
	#	************************ Negative Scenario ***********************
	
	@put_Charges-byAuthorizationTransaction_Twice
	Scenario: Post an Authorization Transaction and tyr to Capture the transaction Twice
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":22.00},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Authorization
		Then response code should be 201
		And I Capture the Authorization transaction with Total Amount 10
		Then response code should be 200
		And I Capture the Authorization transaction with Total Amount 10
		Then response code should be 404
        And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.

	@put_Charges-bySaleTransaction
	Scenario: Post an Sale Transaction and try to Capture the Same transaction
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":22.00},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Sale
		Then response code should be 201
		And I Capture the Sale transaction with Total Amount 70
		Then response code should be 404
        And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.
		
	@put_Charges-byForceTransaction
	Scenario: Post an Force Transaction and try to Capture the Same transaction
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":22.00},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Force
		Then response code should be 201
		And I Capture the Force transaction with Total Amount 70
		Then response code should be 404
        And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.

	@put_Charges-byCreditTransaction
	Scenario: Post an Credit Transaction and try to Capture the Same transaction
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":22.00},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /credits
		Then response code should be 201
		And I Capture the Credit transaction with Total Amount 70
		Then response code should be 404
		And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.
		
	@put_Charges-byInvalidReferenceNumber
    Scenario: Verify the API Put_Charges with Invalid Reference Number
 		Given I set clientId header to `clientId`
        And I set apikey header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set authorization header to `authorization` 
		When Try to Capture the Transaction with Invalid Reference Number /charges/Test123 and Total Amount 60
		Then response code should be 404
        And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.

#	@put_Charges-byBlankReferenceNumber
#	Scenario: Verify the API Put_Charges with Blank Reference Number
#		Given I set clientId header to `clientId`
#		And I set apikey header to `clientId`
#		And I set merchantId header to `merchantId`
#		And I set merchantKey header to `merchantKey`
#		And I set authorization header to `authorization`
#		When Try to Capture the Transaction with Blank Reference Number /charges/ and Total Amount 50
#		Then response code should be 404
#		And response body path code should be 100005
#		And response body path message should be Missing or invalid Application Identifier
#		And response body path detail should be Please contact support for assistance.
		
		
		
		
		