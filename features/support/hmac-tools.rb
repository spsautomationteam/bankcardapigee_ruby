require 'openssl'
require 'json'
require 'base64'
require 'date'


class HmacTools
  def hmac (apiSecret, verb, url, body, merchantId, nonce, timestamp)
    data =  verb + url + body + merchantId.to_s + nonce.to_s + timestamp.to_s
    hashSign = OpenSSL::HMAC.digest("SHA512",apiSecret, data)
    Base64.strict_encode64(hashSign)
  end

  def make_nonce (length)
    possible = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    (0..length - 1).map { (possible).to_a[rand(62)] }.join
  end
end

#hmac = HmacTools.new()

#apiSecret = 'rgpGKka7GTlKixyS'
#verb = 'GET'
#url = 'https://api-qa.sagepayments.com/accountupdater/v1?startdate=20170801&enddate=20170915'
#body = ''
#merchantId = '999999999997'
#nonce = DateTime.now.strftime('%Q')
#t = DateTime.now.strftime('%Q')
#t = t.to_i
#t = t/1000
#timestamp = t.to_s

#hmac = hmac.hmac(apiSecret, verb, url, body, merchantId, nonce, timestamp)

#puts hmac