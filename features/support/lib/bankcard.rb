class Bankcard

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'rest_client'
  require "yaml"
#  require_relative '../../support/API-tools'
  require_relative '../../support/hmac-tools'
  require 'date'


#@base_url
  @reference_num = '0'
  @@content_type =''
  @authorization=''
  @auth=''
  @response=''
  @parsed_response=''

  @domain=''
  @basepath=''
  @clientId=''
  @clientSecret=''
  @merchantId=''
  @merchantKey=''

#constructor,Reading yaml file
  def initialize()
    @hmac = HmacTools.new()


    config = YAML.load_file('config.yaml')
    base_url = config['domain']
    @auth = config['authorization']
    @domain=config['domain']
    @basepath=config['basepath']
    @clientId=config['clientId']
    @clientSecret=config['clientSecret']
    @merchantId=config['merchantId']
    @merchantKey=config['merchantKey']

    puts "@base_url :#{base_url}"
    set_url base_url
  end

  def set_authorization(auth)
    @authorization =auth
  end

  def set_url(url)
    @url = url
  end


  def set_header(header, value)
    puts "header : #{header}, value : #{value}"
    case header
      when "content-type"
        @@content_type =value
      when "Authorization"
        if (value.eql? 'Authorization')
          @authorization =@auth
        else
          @authorization =value
        end
    end
  end


  def set_json_body(jsonbody)
    @json_body = jsonbody
  end

  def postHmac(path)
    post_ref path, ''
  end

  def post_existing_refnum(path)
    post_ref path, @reference_num
  end

  def patch_existing_refnum(path)
    patch_ref path, @reference_num
  end


  def post_ref(path, ref_num)
    full_url= @url+path

    if (!ref_num.empty?)
      full_url +='/'+ref_num
    end

    #Debug urls
    puts "path#{path}"
    puts "@domain#{@domain}"
    url = @domain + path
    puts url
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'POST', url, @json_body, @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.post full_url, @json_body, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp

      @reference_num = JSON.parse(@response.body)['reference']
      @orderNumber = JSON.parse(@response.body)['orderNumber']
      @accountNumber = JSON.parse(@response.body)['accountNumber']
      #   @gatewayId = JSON.parse(@response.body)['gatewayId']
      #  @gatewayId = JSON.parse(@response.body)['gatewayId']
      puts "@orderNumber #{@orderNumber}"
      @total = JSON.parse(@response.body)['total']


      puts "@reference_num: #{@reference_num}"
        # puts "response>>> #{@response}"
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

    # puts "response: #{@response}"

  end

#  *******************************************

  def post_credits_reference(body)

    if body == '/credits/Test123'
      full_url= @url+'/credits/Test123'
      json_body = '{"transactionId": "",     "deviceId": "7894560",     "amount": 20,     "terminalNumber": "" }'
    else
      if body == '/batches/current'
        full_url= @url+'/batches/current'
        json_body = '{"settlementType": "Bankcard"}'
      else
        full_url= @url+'/credits/'+@reference_num
        json_body = body
      end
    end

    #Debug urls
    puts "@domain#{@domain}"
    #    url = @domain + +'/credits/'+@reference_num
    #   puts url
    puts "@full_url: #{full_url}"
    puts "@json_body: #{json_body}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'POST', full_url, json_body, @merchantId, nonce, timestamp)

    puts "hmac #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.post full_url, json_body, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp

      @reference_num = JSON.parse(@response.body)['reference']
      @orderNumber = JSON.parse(@response.body)['orderNumber']
      @accountNumber = JSON.parse(@response.body)['accountNumber']
      #   @gatewayId = JSON.parse(@response.body)['gatewayId']
      #  @gatewayId = JSON.parse(@response.body)['gatewayId']
      puts "@orderNumber #{@orderNumber}"
      @total = JSON.parse(@response.body)['total']


      puts "@reference_num: #{@reference_num}"
        # puts "response>>> #{@response}"
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

    # puts "response: #{@response}"

  end

#  *******************************************

#####################################################

  def post_lineitems(body)

    if body == '/charges/Test123/lineitems'
      full_url= @url+'/charges/Test123/lineitems'
      lineitem_body = '{"visa": [{"commodityCode": "2","itemDescription": "dfgdfg","productCode": "5","quantity": 4,"unitOfMeasure": "0","unitCost": 40,"vatTaxAmount": 4,"vatTaxRate": 1,"discountAmount": 5,"lineItemTotal": 35}]}'
    else
      full_url= @url+'/charges/'+@reference_num+'/lineitems'
      lineitem_body = body
    end

    puts "lineitem_body: #{lineitem_body}"

    #Debug urls
    puts "@domain#{@domain}"
    # url = @domain +'/charges/'+@reference_num+'/lineitems'
    # puts url
    puts "@full_url: #{full_url}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'POST', full_url, lineitem_body, @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.post full_url, lineitem_body, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

    # puts "response: #{@response}"

  end

#  *******************************************

  def get_listof_transactions(type)

    full_url= @url+'/charges?type='+type

    #Debug urls
    puts "type#{type}"
    puts "@domain#{@domain}"
    url = @domain +'/charges?type='+ type
    puts "@get_url: #{url}"
    puts "@get_full_url: #{full_url}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', url, '', @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.get full_url, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

    # puts "response: #{@response}"

  end

#  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  def get_current_transdetails()
    full_url= @url+'/charges?reference='+@reference_num

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    url = @domain +'/charges?reference='+ @reference_num
    puts "@get_url: #{url}"
    puts "@get_full_url: #{full_url}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', url, '', @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.get full_url, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

#  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  def get_current_credit_transdetails(path)

    if path == '/credits/'
      full_url= @url+'/credits/'+@reference_num
    else
      full_url= @url+'/credits?reference='+@reference_num
    end

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    # url = @domain +'/credits?reference='+@reference_num
    # puts "@get_url: #{url}"
    puts "@get_full_url: #{full_url}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', full_url, '', @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.get full_url, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

###########################################

  def get_current_transdetails_by_refnumber(method)

    if method == '/charges/{reference}/lineitems'
      full_url= @url+'/charges/'+@reference_num+'/lineitems'
    else
      if method == '/charges/Test123/lineitems'
        full_url= @url+'/charges/Test123/lineitems'
      else
        full_url= @url+method+@reference_num
      end
    end

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    #  url = @domain +method+ @reference_num
    #  puts "@get_url: #{url}"
    puts "@get_full_url: #{full_url}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', full_url, '', @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.get full_url, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

###########################################

  def get_current_credit_transdetails_by_refnumber()
    full_url= @url+'/credits/'+@reference_num

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    url = @domain +'/credits/'+ @reference_num
    puts "@get_url: #{url}"
    puts "@get_full_url: #{full_url}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', url, '', @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.get full_url, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

###########################################

  def captureAuthorization(amount)

    full_url= @url+'/charges/'+@reference_num

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    url = @domain +'/charges/'+ @reference_num
    puts "@get_url: #{url}"
    puts "@get_full_url: #{full_url}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    # amount = amount.to_i

    put_body = '{ "amounts": { "tip": 4.24, "total": '+amount+',  "tax": 2.12, "shipping": 1.06 } }'

    puts "capture body #{put_body}"

    @authorization = @hmac.hmac(@clientSecret, 'PUT', url, put_body, @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.put full_url, put_body, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

#  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  def captureAuthorization_byInvalid_refrencenumber(path, amount)

    full_url= @url+path

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    url = @domain +path
    puts "@get_url: #{url}"
    puts "@get_full_url: #{full_url}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    put_body = '{ "amounts": { "tip": 4.24, "total": '+amount+',  "tax": 2.12, "shipping": 1.06 } }'

    puts "capture body #{put_body}"

    @authorization = @hmac.hmac(@clientSecret, 'PUT', url, put_body, @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.put full_url, put_body, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

#  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  def get_by_paramvalue(path)

    if path == '/batches'
      full_url= @url+path
    else
      if path == '/batches/current'
        full_url= @url+path
      else
        if path == '/batches/'
          full_url= @url+path+@reference_num
        else
          if path == '/batches/{reference}?pageSize=2'
            full_url= @url+'/batches/'+@reference_num+'?pageSize=2'
          else
            if path == '/batches/{reference}?pageNumber=1'
              full_url= @url+'/batches/'+@reference_num+'?pageNumber=1'
            else
              if path == '/batches/Test123'
                full_url= @url+'/batches/Test123'
              else
                full_url= @url+path
              end
            end
          end
        end
      end
    end

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    puts "@get_full_url: #{full_url}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'GET', full_url, '', @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.get full_url, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

#####################################################

  def delete_transaction_byreference(path)

    if path == '/charges'
      full_url= @url+'/charges/'+@reference_num
    else
      if path == '/credits'
        full_url= @url+'/credits/'+@reference_num
      else
        if path == '/charges/{reference}/lineitems'
          full_url= @url+'/charges/'+@reference_num+'/lineitems'
        else
          if path == '/charges/Test123/lineitems'
            full_url= @url+'/charges/Test123/lineitems'
          else
            full_url= @url+path
          end
        end
      end
    end

    #Debug urls
    puts "get_reference_num#{@reference_num}"
    puts "@domain#{@domain}"
    puts "@get_full_url: #{full_url}"

    nonce = DateTime.now.strftime('%Q')
    timestamp = DateTime.now.strftime('%Q').to_i/1000

    contenttype = "application/json"

    puts "@@content_type #{contenttype}"
    puts "@clientSecret #{@clientSecret}"
    puts "@@merchantId #{@merchantId}"
    puts "@merchantKey #{@merchantKey}"
    puts "@nonce #{nonce}"
    puts "@timestamp #{timestamp}"

    @authorization = @hmac.hmac(@clientSecret, 'DELETE', full_url, '', @merchantId, nonce, timestamp)

    puts "authorization #{@authorization}"
    #Do post action and get response
    begin

      @response = RestClient.delete full_url, :merchantId => @merchantId, :merchantKey => @merchantKey, :clientId => @clientId, :Authorization => @authorization, :'Content-Type' => contenttype, :nonce => nonce, :timestamp => timestamp
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end


#  ********************************


  def patch(path)
    patch_ref path, ''
  end


  def patch_ref(path, ref_num)

    full_url= @url+path
    if (!ref_num.empty?)
      full_url +='/'+ref_num
      puts "ref_num:#{ref_num}"
    end

    puts "#patch_ref "
    #Debug urls
    puts "path#{path}"
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@@content_type: #{@@content_type}"


    #Do post action and get response
    begin
      @response = RestClient.patch full_url, @json_body, :Authorization => @authorization, :Content-Type => @@content_type
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end


    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end


  def verify_response_params_contain(response_param, value)
    puts "response_param:#{response_param} , value:#{value}"
    if @parsed_response[response_param].nil?
      expect(@parsed_response[response_param].include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"
    else
      expect(@parsed_response[response_param].to_s.delete('').include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
    end
  end

  def verify_response_for_refnumber()
    if !@response.nil?
      expect(@response.include? @reference_num).to be_truthy, "Expected : #{@reference_num} ,got : #{@response} "
    else
      expect(false).to be_truthy, "Expected : #{@reference_num} ,got : Empty response}"
    end
  end

  def verify_response_value_contain(response_value)
    puts "response_value:#{response_value}"
    if !@response.nil?
      expect(@response.include? response_value).to be_truthy, "Expected : #{response_value} ,got : #{@response} "
    else
      expect(false).to be_truthy, "Expected : #{response_value} ,got : Empty response}"
    end
  end

  def verify_regex(val, exp)
    if (exp.include? '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
      regex = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/
    else if (exp.include? '\d{1,3}')
        regex = /^\d{1,3}$/
      end
    end
    puts "val#{val}"
    resp_val =@parsed_response[val]
    puts "resp_val#{resp_val}"

    status = resp_val.match?(regex)
    puts "status #{status}"

    expect(status).to be_truthy, "#{val} validation failed"

  end

  def verify_response_contain_refnumber()
    puts "@reference_num #{@reference_num}"
    if !@response.nil?
      expect(@response.include? @reference_num).to be_truthy, "Expected : #{@reference_num} ,got : #{@response} "
    else
      expect(false).to be_truthy, "Expected : #{@reference_num} ,got : Empty response}"
    end
  end

  def verify_response_value_not_contain(response_value)
    puts "response_value:#{response_value}"
    if !@response.nil?
      expect(@response.include? response_value).to be_falsey, "Expected : #{response_value} ,got : #{@response} "
    else
      expect(true).to be_falsey, "Expected : #{response_value} ,got : Empty response}"
    end
  end


  def verify_response_empty
    expect(@response.empty?).to be_truthy, "Expected : Response  empty ,got : #{@response} \nResponesbody:#{@parsed_response}"
  end

  def verify_response_code resp_code
    expect(@response.code.to_s).to eql(resp_code), "Expected : #{resp_code} ,got : #{@response.code.to_s} \nResponesbody:#{@parsed_response}"
  end

  def verify_response_params(response_param, value)
    puts "response_param:#{response_param} , value:#{value}"

    if response_param.include? "."
      response_param1 = response_param.split('.').first
      response_param2 = response_param.split('.').last
      #puts "response_param1:#{response_param1} ,response_param2#{response_param2}"
      expected_val =@parsed_response[response_param1][response_param2]
      if expected_val.nil?
        expect(expected_val).to eql(value), "Expected : #{value} ,got : #{expected_val} \nResponesbody:#{@parsed_response}\n"
      else
        # puts expected_val.to_s.strip.length
        expect(expected_val.to_s.strip).to eql(value), "Expected : #{value} ,got : #{expected_val.to_s.strip} \nResponesbody:#{@parsed_response}\n"
      end
    else

      if @parsed_response[response_param].nil?
        expect(@parsed_response[response_param]).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"

      else
        expect(@parsed_response[response_param].to_s.delete('')).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
      end

    end
  end

  def verify_response_headers(header_param, value)
    puts "header_param:#{header_param} , value:#{value}"
    expect(@response.headers[:content_type]).to include(value), "Expected : #{value} ,got : #{@response.headers[:content_type]} \nResponesbody:#{@parsed_response}"
  end


end