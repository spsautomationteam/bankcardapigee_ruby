@get_Batches_Current
Feature: Get Batches Current
	As an API consumer
	I want to query batches requests
	So that I know they have been processed

	@get-Batches-Current_AllBatchTransactions
    Scenario: Used to retrieve itemized detail about the transactions in the current batch.
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Sale
	    Then response code should be 201
		And I set body to { "settlementType": "Bankcard"}
		When I use HMAC and POST to /batches/current
		Then response code should be 201
		And response body path status should be Approved
		And response body path message should be BATCH 1 OF 1 CLOSED
		And I use HMAC then GET List of Existing Current Settled Batches Transactions using /batches/current
	#	And I get List of Existing Current Batche Transactions
        Then response code should be 200
	#	And response body should contain the same reference number
        And response header Content-Type should be application/json	
		And response body path should contain "reference"
		And response body path should contain "service"
		And response body path should contain "billing"
		And response body path should contain "shipping"
		And response body path should contain "customer"
		And response body path should contain "paymentType"
		
	@get-Batches-Current_byPageSize
    Scenario: Verify the API Get batches with PageSize
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /batches/current?PageSize=2
        Then response code should be 200
        And response body path should contain "pageSize":2
		And response body path should contain "reference"
		And response body path should contain "service"
		And response body path should contain "billing"
		And response body path should contain "shipping"
		And response body path should contain "customer"
		And response body path should contain "paymentType"
	#	And response body path $.items[1].service should be Bankcard
		And response body path should contain "service":"Bankcard"
			
	@get-Batches-Current_byPageNumber
    Scenario: Verify the API Get batches with PageNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /batches/current?pageNumber=1
        Then response code should be 200
        And response body path should contain "pageNumber":1
		And response body path should contain "reference"
		And response body path should contain "service"
		And response body path should contain "billing"
		And response body path should contain "shipping"
		And response body path should contain "customer"
		And response body path should contain "paymentType"
			
	
   