@get_Batches_Current_Summary
Feature: Get_Batches_Current_Summary
	As an API consumer
	I want to query batches requests
	So that I know they have been processed
		
	@get_Batches_Current_Summary_AllSummaryTransactions
    Scenario: Used to retrieve summarized information, such as count and volume, about the transactions in the current batch.
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /batches/current/summary
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain "paymentType":"Discover"
		And response body path should contain "paymentType":"MasterCard"
		And response body path should contain "paymentType":"Visa"
		And response body path should contain "paymentType":"American Express"
		And response body path should contain "authCount"
		And response body path should contain "authTotal"
		And response body path should contain "saleCount"
		And response body path should contain "saleTotal"
		And response body path should contain "creditCount"
		And response body path should contain "creditTotal"
		And response body path should contain "totalCount"
		And response body path should contain "totalVolume"
		
		
		############# Negative Scenarios  #####################		
		
		
#	@get_Batches_Current_Summary_byinValidReference
#	Scenario: Try to get transaction summery with invalid Reference
#        Given I have valid merchant credentials
#		When I use HMAC and GET /batches/Test1234/summary
#		Then response code should be 200
#		And response body should contain "authCount":0
#		And response body should contain "authTotal":0
#		And response body should contain "saleCount":0
#		And response body should contain "saleTotal":0
#		And response body should contain "creditCount":0
#		And response body should contain "creditTotal":0
#		And response body should contain "totalCount":0
#		And response body should contain "totalVolume":0
		
		
