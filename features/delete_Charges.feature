@Charges_Delete
Feature: Void an Existing Charge Transaction
	As an API consumer
	I want to Void an Existing Charge Transaction

	@Delete_Charges-Sale_Auth_ForceTransactions
	Scenario Outline: Perform a transaction and then delete that Same transaction
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
#		When I Delete the same transaction by reference Number
		When I use HMAC and DELETE to /charges
		Then response code should be 200
		Examples:
		|	TypeOfTransaction	|
		|	Sale				|
		|	Authorization		|
		|	Force				|
		
		
	#  ********************** Negative Scenarios ***********************
		
	@Delete_Charges_byDeleteTransactions_Twice
	Scenario Outline: Perform a transaction and then delete that Same transaction twice
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
		When I use HMAC and DELETE to /charges
		Then response code should be 200
		When I use HMAC and DELETE to /charges
		Then response code should be 404
		And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.
		Examples:
			|	TypeOfTransaction	|
			|	Sale				|
			|	Authorization		|
			|	force				|

			
#	@Delete_Charges_byBlankReferenceNumber
#    Scenario: Verify the API Delete_Charges with Blank Reference Number
#        Given I set clientId header to `clientId`
#        And I set apikey header to `clientId`
#        And I set merchantId header to `merchantId`
#        And I set merchantKey header to `merchantKey`
#		And I set authorization header to `authorization`  
#	    When Verify Delete Charges with Blank Reference Number
#	   When I use HMAC and GET /charges/
#        Then response code should be 201
#		And response body path $.message should be Internal Server Error
#		And response body path $.detail should be Please contact support for assistance.
		
	@Delete_Charges_byInvalidReferenceNumber
    Scenario: Verify Delete_Charges with Invalid Reference Number
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and DELETE to /charges/Test123
        Then response code should be 404
		And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.
		
		
