@charges_POST
Feature: Post Charges
	As an API consumer
	I want to create charge requests
	So that I know they can be processed

	@post-Charges-Sale-WithParameterize
    Scenario Outline: Post a sale
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "transactionId": "",     "<Type>": {         "amounts": {             "tip": 4.24,             "total": <Amount>,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "<OrderNumber>",         "cardData": {             "number": "<CardNumber>",             "expiration": "1219", "cvv":"123"         },         "customer": {             "email": "<Email>",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": <IsRecurring>      } }
	#	And I set body to {     "transactionId": "",     "<Type>": {         "amounts": {             "tip": 4.24,             "total": <Amount>,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "<OrderNumber>",         "cardData": {             "number": "<CardNumber>",             "expiration": "1219" },         "customer": {             "email": "<Email>",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": <IsRecurring>      } }
         #,"cvv":"123"
		When I use HMAC and POST to /charges?type=sale
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
		Examples:
		|Type       |CardNumber 	   |Amount  |OrderNumber | CustomerName  | Email 			|IsRecurring |
	#	|retail     |2223000010232012  | 44.50  |1234566     | San Kumar     |vinayak@foo.com   |false |
		|retail     |4111111111111111  | 45.52  |1234567     | Midhun Kumar  |midhun@foo.com    |false |
		|eCommerce  |5499740000000057  | 46.13  |1234568 	 | Midhun Kumar  |midhun@test.com   |true  |
		|healthcare |6011000993026909  | 47.14  |1234569 	 | Sateesh Kumar |sateesh@gmail.com |false |
		|eCommerce  |371449635392376   | 48.15  |1234560 	 | Bharath Kumar |bharath@yahoo.com |true  |
	
	@post-Charges-SaleRecurring	
	Scenario: Post a sale and make it a recurring action
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":11.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Sale
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
		And response body path message should contain APPROVED

	@post-Charges-Auth-WithParameterize
    Scenario Outline: Post an Auth transaction
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"		
		And I set body to {     "transactionId": "",     "<Type>": {         "amounts": {             "tip": 4.24,             "total": <Amount>,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "<OrderNumber>",         "cardData": {             "number": "<CardNumber>",             "expiration": "1220", "cvv":"1234"        },         "customer": {             "email": "<Email>",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": <IsRecurring>      } }
	#	And I set body to {     "transactionId": "",     "<Type>": {         "amounts": {             "tip": 4.24,             "total": <Amount>,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "<OrderNumber>",         "cardData": {             "number": "<CardNumber>",             "expiration": "1220"   },         "customer": {             "email": "<Email>",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": <IsRecurring>      } }	
	   #,"cvv":"123"
		When I use HMAC and POST to /charges?type=Authorization
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
		And response body path message should contain APPROVED
		
		Examples:
		|Type       |CardNumber 	   |Amount  |OrderNumber | CustomerName  | Email 			|IsRecurring |
		|retail     |4111111111111111  | 51.11  |1234567     | San Kumar     |vinayak@foo.com   |false |
		|eCommerce  |5499740000000057  | 52.22  |1234568 	 | Midhun Kumar  |midhun@test.com   |true  |
		|healthcare |6011000993026909  | 53.33  |1234569 	 | Sateesh Kumar |sateesh@gmail.com |false |
		|eCommerce  |371449635392376   | 54.44  |1234560 	 | Bharath Kumar |bharath@yahoo.com |true  |
		
	@post-Charges-AuthRecurring
    Scenario: Post an Auth transaction and make it a recurring action
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":44.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Authorization
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
		And response body path message should contain APPROVED
		
	@post-Charges-Force-WithParameterize
    Scenario Outline: Post a Force
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
	#	And I set body to {"transactionId": "tid-1234", "ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "1216", "Number": "4111111111111111", "cvv":"123" } } }		
		And I set body to {     "transactionId": "",     "<Type>": {         "amounts": {             "tip": 4.24,             "total": <Amount>,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "<OrderNumber>",         "cardData": {             "number": "<CardNumber>",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "<Email>",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": <IsRecurring>      } }
		When I use HMAC and POST to /charges?type=force
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
		And response body path message should contain APPROVED
		
		Examples:
		|Type       |CardNumber 	   |Amount  |OrderNumber | CustomerName  | Email 			|IsRecurring |
		|retail     |4111111111111111  | 31.50  |1234567     | San Kumar     |vinayak@foo.com   |false |
		|eCommerce  |5499740000000057  | 32.12  |1234568 	 | Midhun Kumar  |midhun@test.com   |true  |
		|healthcare |6011000993026909  | 33.12  |1234569 	 | Sateesh Kumar |sateesh@gmail.com |false |
		|eCommerce  |371449635392376   | 44.12  |1234560 	 | Bharath Kumar |bharath@yahoo.com |true  |
		
	@post-Charges-ForceRecurring
    Scenario: Post an Auth transaction and make it a recurring action
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":77.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Force
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
		And response body path message should contain APPROVED
		
	@post-Charges-SaleAuthForce_Parameterize
    Scenario Outline: Post a Sale, Authorization and Force transactions
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "transactionId": "",     "<Type>": {"amounts": {"tip": 4.24, "total": <Amount>, "tax": 2.12, "shipping": 1.06 }, "authorizationCode": "464646", "orderNumber": "7687677", "cardData": {  "number": "<CardNumber>", "expiration": "1220", "cvv":"123" }, "customer": { "email": "vinayak@foo.com",  "telephone": "7033334444", "fax": "7033334444" }, "billing": { "name": "Vinayak Bhat",  "address": "123Main St.", "city": "Reston", "state": "VA",  "postalCode": "20190", "country": "US" }, "shipping": {  "name": "Vinayak Bhat", "address": "123Main St.", "city": "Reston",   "state": "VA",  "postalCode": "20190",   "country": "US"  },  "isRecurring": false      } }
		When I use HMAC and POST to /charges?type=<TransationType>
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path status should be Approved
		And response body path message should contain APPROVED
		
		Examples:
		|Type      |TransationType  |CardNumber 	  |Amount  |
		|retail    | Sale           |4111111111111111 | 44.81  |
		|eCommerce | Authorization  |5499740000000057 | 44.82  |
		|healthcare| Force          |6011000993026909 | 46.83  |
		|eCommerce | Force          |371449635392376  | 47.84  |
		
		
		# **************  Negative Scenarios  *************************
		
	@post-Charges-BlankTransactionType_Sale
    Scenario: Verify post charges with Blank Transaction type Sale
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":88.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=
        Then response code should be 400
        And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request: The TransactionCode field is required.
		
	@post-Charges-BlankTransactionType_Force
    Scenario: Verify post charges with Blank Transaction type Force
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":19.10},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=
        Then response code should be 400
        And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request: The TransactionCode field is required.
		
	@post-Charges-BlankTransactionType_Authorization
    Scenario: Verify post charges with Blank Transaction type Authorization
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":25.50},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=
        Then response code should be 400
        And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request: The TransactionCode field is required.

	@post-Charges-Sale-byInvalidAmount
    Scenario: Post a sale for invalid Amount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234", "ECommerce": { "Amounts": { "Total": -48.0 },"authorizationCode": "123456","CardData": { "Expiration": "1216", "Number": "4111111111111111", "cvv":"123" } } }		
		When I use HMAC and POST to /charges?type=sale
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.Amounts: Invalid Total Amount
		
	@post-Charges-Sale-byBlankAmount
    Scenario: Post a sale for invalid Amount
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234", "ECommerce": { "authorizationCode": "123456","CardData": { "Expiration": "1216", "Number": "4111111111111111", "cvv":"123" } } }		
		When I use HMAC and POST to /charges?type=sale
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce: The Amounts field is required.
				
	@post_Charges-byBlankCardNumber
    Scenario: Post a sale for Blank Card Number
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234", "ECommerce": { "Amounts": { "Total": 11.0 },"authorizationCode": "123456","CardData": {"Number": "", "Expiration": "1216", "cvv": "123" } } }
		When I use HMAC and POST to /charges?type=sale
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The Number field is required.

	@post-Charges-Sale-byInvalidCardNumber
    Scenario: Post a sale for invalid CardNumber
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": {"Number": "9999999999999999", "Expiration": "1216", "cvv": "123" } } }		
		When I use HMAC and POST to /charges?type=sale
        Then response code should be 401

		And response body path code should be 100008
		And response body path message should be Invalid message request content
		And response body path detail should be You are using certification environment which is restricted to process transaction using test Credit cards only.

	#	And response body path code should be 400000
	#	And response body path message should be There was a problem with the request. Please see 'detail' for more.
	#	And response body path detail should be InvalidRequestData : INVALID C_CARDNUMBER
		
	@post-Charges-Sale-byInvalidCVV
    Scenario: Post a sale for invalid CVV
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": {"Number": "4111111111111111", "Expiration": "1216", "cvv": "@#$%*@#$*" } } }		
		When I use HMAC and POST to /charges?type=sale
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The field CVV must be a string or array type with a maximum length of '4'.
		
	@post-Charges-Sale-byCVVMoreThan4Digits
    Scenario: Post a sale by CVV value More Than 4 Digits 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Number": "4111111111111111", "Expiration": "1216", "cvv": "12345" } } }		
		When I use HMAC and POST to /charges?type=sale
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The field CVV must be a string or array type with a maximum length of '4'.
     
	@post-Charges-Sale-byInvalidEmailId
    Scenario: Post a sale for invalid EmailId
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 4.24,             "total": 42.42,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "sankumar.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
		When I use HMAC and POST to /charges?type=Sale
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.Retail.Customer: The Email field is not a valid e-mail address.
		
	@post-Charges-Sale-byBlankEmailId
    Scenario: Post a sale for Blank EmailId
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 4.24,             "total": 42.42,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
		When I use HMAC and POST to /charges?type=Sale
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.Retail.Customer: The Email field is not a valid e-mail address.
		
	@post-Charges-Sale-byInvalidExpDate
    Scenario: Post a sale for invalid Expiry Date
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "20-12", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /charges?type=Sale
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.
		
	@post-Charges-Sale_byExpDateLessThan4Digits
    Scenario: Post a sale by Expire Date Less Than 4 Digits
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "20", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /charges?type=Sale
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.
		
	@post-Charges-Sale_byExpDateGreaterThan4Digits
    Scenario: Post a sale by Expire Date Greater Than 4 Digits
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "20255", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /charges?type=Sale
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.

	@post-Charges-Sale-byBlankExpireDate
	Scenario: Post a sale for Blank Expiry Date
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "", "Number": "4111111111111111" } } }
		When I use HMAC and POST to /charges?type=Sale
		Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: The Expiration field is required.; The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.

	@post-Charges-Sale-ExpireDateWithNonDigits
    Scenario: Post a sale forExpiry Date with Non Digits
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "abcd", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /charges?type=Sale
        Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : request.ECommerce.CardData: Expiration must be a number

#	@post-Charges-SaleSandBox123
 #   Scenario: Post a sale for sandbox
#		Given I have valid credentials
 #       And I have a valid request
  #      And I generate an authKey
	#	And I set body to {"retail": {"amounts": {"total": 8.1},"authorizationCode": "565656", "orderNumber": "5656","cardData": {"number": "4111111111111111","expiration": "0618"}}}
#		When I use HMAC and POST to /charges?type=sale
 #       Then response code should be 201
  #      And response body should be valid json
   #     And response body should contain APPROVED

		
		