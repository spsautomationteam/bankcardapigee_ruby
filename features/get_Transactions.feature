@Get_Transactions
Feature: Get_Transactions
	As an API consumer
	I want to query transactions requests
	So that I know they have been processed

	@get-All-Transactions
	Scenario Outline: query to get all Transactions details
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
	#	When I use HMAC and then GET a list of existing <TypeOfTransaction> transaction
		When I use HMAC and GET /transactions?type=<TypeOfTransaction>
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body should contain above performed transaction reference
		And response body path should contain "service":"Bankcard"

		Examples:
			|	TypeOfTransaction	|
			|	Sale				|
			|	Authorization		|
			|	force				|

	@get-Transactions_GetSameTransactionDetails_ByReferenceNum
	Scenario Outline: query for Get Same Transaction Details By ReferenceNum
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
		And I get that same transaction details by using reference number and /transactions?reference=
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should be contain same reference number
		And response body path should contain "service":"Bankcard"

		Examples:
			|	TypeOfTransaction	|
			|	Sale				|
			|	Authorization		|
			|	force			    |

	@get-Transactions_byTransType
	Scenario Outline: Verify the API Get transactions with TransactionCode
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
	#	And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
	#	When I use HMAC and POST to /charges?type=<TransactionType>
	#	Then response code should be 201
		When I use HMAC and GET /transactions?type=<TransactionType>
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "type":"<TransactionType>"
		And response body path should contain "service":"Bankcard"

		Examples:
			|TransactionType  |
			|Sale			  |
			|Authorization 	  |
			|Force 			  |
		#	|Void 			  |
			|Credit 		  |
        #   |CreditByReference|
		    |Capture		  |


	@get-Transactions_byStartDate
	Scenario: Verify the API Get transactions with StartDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?startDate=2017-05-17
		Then response code should be 200
		And response body path should contain "startDate":"2017-05-17"
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"

	@get-Transactions_byEndDate
	Scenario: Verify the API Get transactions with EndDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?endDate=2017-10-22
		Then response code should be 200
		And response body path should contain "endDate":"2017-10-22"
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"

	@get-Transactions_byPageSize
	Scenario: Verify the API Get transactions with PageSize
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?PageSize=1
		Then response code should be 200
		And response body path should contain "pageSize":1
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"

	@get-Transactions_byPageNumber
	Scenario: Verify the API Get transactions with PageNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?pageNumber=2
		Then response code should be 200
		And response body path should contain "pageNumber":2
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"

	@get-Transactions_byIsPurchaseCard
	Scenario Outline: Verify the API Get transactions with IsPurchaseCard
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?IsPurchaseCard=<IsPurchaseCard>
		Then response code should be 200
		And response body path should contain "isPurchaseCard":<IsPurchaseCard>
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"

		Examples:
			|IsPurchaseCard    |
		#	|true    		   |
			|false             |

	@get-Transactions_byGatewayID
	Scenario: Verify the API Get transactions with GatewayID
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Force
		Then response code should be 201
		When I use HMAC and GET /transactions?GatewayId=999999999997
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		And response body path should contain "gatewayId":"999999999997"

	@get-Transactions_byReferenceNumber
	Scenario: Verify the API Get transactions with Bankcard ReferenceNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Authorization
		Then response code should be 201
		When I get that same transaction details
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"

	@get-Transactions_byOrderNumber
	Scenario: Verify the API Get transactions with OrderNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Force
		Then response code should be 201
		When I use HMAC and GET /transactions?OrderNumber=IDBRHEIqFd
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		And response body path should contain "orderNumber":"IDBRHEIqFd"

	@get-Transactions_byTotalAmount
	Scenario: Verify the API Get transactions with TotalAmount
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":10.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Force
		Then response code should be 201
		When I use HMAC and GET /transactions?total=10
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		And response body path should contain "total":10

	@get-Transactions_byStatus
	Scenario Outline: Verify the API Get transactions with Status
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":10.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Sale
		Then response code should be 201
		When I use HMAC and GET /transactions?status=<Status>
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		And response body path should contain "status":"<Status>"

		Examples:
			|Status  |
			|Declined|
			|Batch   |
			|Settled |
		#	|Expired |

  #	commneted below scenario because of no transactions done through Mobile
	@get-Transactions_bySource
	Scenario Outline: Verify the API Get transactions with Source
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":10.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Sale
		Then response code should be 201
		When I use HMAC and GET /transactions?source=<Source>
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "applicationName":"<Value>"

		Examples:
			|Source    | Value |
		#	| Mobile   | SPS Mobile    |
			|Recurring | SPS Recurring |

	@get-Transactions_byAccountNumber
	Scenario Outline: Verify the API Get transactions with AccountNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":10.0},"CardData":{"Expiration":"1122","Number":"<cardNumber>", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Sale
		Then response code should be 201
		When I use HMAC and GET /transactions?accountNumber=<number>
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "accountNumber":"<accountNumber>"
		Examples:
			|cardNumber          | number | accountNumber |
			|4111111111111111    | 1111   | XXXXXXXXXXXX1111 |
			|5454545454545454    | 5454   | XXXXXXXXXXXX5454 |
			|6011000993026909    | 6909   | XXXXXXXXXXXX6909 |
			|371449635392376     | 2376   | XXXXXXXXXXX2376  |

	@get-Transactions_byApprovalCode
	Scenario: Verify the API Get transactions with ApprovalCode or Code
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Force
		Then response code should be 201
		When I use HMAC and GET /transactions?approvalCode=213213
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		And response body path should contain "responseCode":"213213"


  # +++++++++++++++++    Negative Scinarios   ++++++++++++++++++++++++++

	@get-Transactions_byinValidReference
	Scenario: Verify the API Get transactions with inValidReference
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?reference=fksjldfdsljfs
		Then response code should be 200
		And response body path should not contain "reference":"fksjldfdsljfs"
		And response body path should not contain "service":"Bankcard"
		And response body path should not contain "reference"

	@get-Transactions_byFutureDate_AsStartDate
	Scenario: Verify the API Get transactions with Future StartDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?StartDate=2030-12-12
		Then response code should be 200
		And response body path should contain "startDate":"2030-12-12"
   #	And response body path should not contain "reference"
 	#	And response body path should not contain "service"

	@get-Transactions_byPastDate_AsEndDate
	Scenario: Verify the API Get Charges with Past EndDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?EndDate=2012-12-12
		Then response code should be 200
		And response body path should contain "endDate":"2012-12-12"
 	#	And response body path should not contain "reference"
 	#	And response body path should not contain "service"

	@get-Transactions_byinValidApprovalCode
	Scenario: Verify the API Get transactions with inValid ApprovalCode
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?approvalCode=Test1234
		Then response code should be 200
		And response body path should not contain "responseCode":"Test1234"
		And response body path should not contain "reference"
		And response body path should not contain "service"

	@get-Transactions_byinValidOrderNumber
	Scenario: Verify the API Get transactions with inValidOrderNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?orderNumber=Test123
		Then response code should be 200
		And response body path should not contain "orderNumber":"Test123"
		And response body path should not contain "reference"
		And response body path should not contain "service"

	@get-Transactions_byinValidStatus
	Scenario: Verify the API Get transactions with inValidStatus
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?Status=Test123
		Then response code should be 200
		And response body path should not contain "status":"Test123"
 	#	And response body path should not contain "reference"
 	#	And response body path should not contain "service"

	@get-Transactions_byinValidSource
	Scenario: Verify the API Get transactions with inValidSource
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?Source=Test123
		Then response code should be 200
		And response body path should not contain "source":"Test123"
 	#	And response body path should not contain "reference"
 	#	And response body path should not contain "service"

	@get-Transactions_byinValidIsPurchaseCard
	Scenario: Verify the API Get transactions with inValidPurchaseCard
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions?IsPurchaseCard=Test123
		Then response code should be 200
		And response body path should not contain "source":"Test123"
 	#	And response body path should not contain "reference"
 	#	And response body path should not contain "service"


