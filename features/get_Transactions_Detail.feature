@get_Transactions_Detail
Feature: Get Transactions Detail
	As an API consumer
	I want to query charge requests
	So that I know they have been processed

	@get-Transactions-Details_Get-All-Transactions
	Scenario Outline: query to get all Transactions details
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
		And I get that same transaction details by using reference number and /transactions/
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should be contain same reference number
		And response body path should contain "type":"<TypeOfTransaction>"
		And response body path should contain "service":"Bankcard"

		Examples:
			|	TypeOfTransaction	|
			|	Sale				|
			|	Authorization		|
			|	Force				|

	@get_Transactions_Detail-byValidRefNumber_Credit
    Scenario: Verify the API get_Transactions_Detail with Credit ReferenceNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /credits
		Then response code should be 201
		And I get that same transaction details by using reference number and /transactions/
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should be contain same reference number
		And response body path should contain "type":"Credit"
		And response body path should contain "service":"Bankcard"
		
		
   #	************ Negative Scenarios **********************
	  
	@get_Transactions_Detail_InvalidReferenceNumber
    Scenario: Verify the API get_Transactions_Detail with invalid ReferenceNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /transactions/Test123
        Then response code should be 404
		And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.
		
		
		
		
		
		
		
		