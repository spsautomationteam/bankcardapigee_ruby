@Get_Charges
Feature: Get_Charges
	As an API consumer
	I want to query charge requests
	So that I know they have been processed

	@get-All-Charges
    Scenario Outline: query all charges
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
		When I use HMAC and then GET a list of existing <TypeOfTransaction> transaction
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body should contain above performed transaction reference
		And response body path should contain "service":"Bankcard"

		Examples:
		|	TypeOfTransaction	|
		|	Sale				|
		|	Authorization		|
		|	force				|

	@get-Charges_GetSameTransactionDetails
	Scenario Outline: query for Get Same Transaction Details
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
		And I get that same transaction details by using reference number and /charges/
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should be contain same reference number
		And response body path should contain "service":"Bankcard"

		Examples:
			|	TypeOfTransaction	|
			|	Sale				|
			|	Authorization		|
			|	force				|

	@get-Charges_byTransType
	Scenario Outline: Verify the API Get charges with TransactionCode
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TransactionType>
		Then response code should be 201
		When I use HMAC and GET /charges?type=<TransactionType>
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "type":"<TransactionType>"
		And response body path should contain "service":"Bankcard"

		Examples:
			|TransactionType  |
			|Sale			  |
			|Authorization 	  |
			|Force 			  |
		  # |Void 			  |
			|Credit 		  |
		  # |CreditByReference|
          # |Capture		  |
	
		
	@get-Charges_byStartDate
    Scenario: Verify the API Get charges with StartDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /charges?startDate=2017-05-17
        Then response code should be 200
        And response body path should contain "startDate":"2017-05-17"
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		
	@get-Charges_byEndDate
    Scenario: Verify the API Get charges with EndDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /charges?endDate=2017-10-22
        Then response code should be 200
        And response body path should contain "endDate":"2017-10-22"
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		
	@get-Charges_byPageSize
    Scenario: Verify the API Get charges with PageSize
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /charges?PageSize=1
        Then response code should be 200
        And response body path should contain "pageSize":1
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
			
	@get-Charges_byPageNumber
    Scenario: Verify the API Get charges with PageNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /charges?pageNumber=2
        Then response code should be 200
        And response body path should contain "pageNumber":2
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		
	@get-Charges_byIsPurchaseCard
    Scenario Outline: Verify the API Get charges with IsPurchaseCard
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
        When I use HMAC and GET /charges?IsPurchaseCard=<IsPurchaseCard>
        Then response code should be 200
        And response body path should contain "isPurchaseCard":<IsPurchaseCard>
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		
		Examples:
		|IsPurchaseCard    |
	#	|true    		   |
		|false             |
		
	@get-Charges_byGatewayID
    Scenario: Verify the API Get charges with GatewayID
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Force
		Then response code should be 201
        When I use HMAC and GET /charges?GatewayId=999999999997
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		And response body path should contain "gatewayId":"999999999997"
		
	@get-Charges_byReferenceNumber
    Scenario: Verify the API Get charges with Bankcard ReferenceNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Authorization
		Then response code should be 201
		When I get that same transaction details
        Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"

	@get-Charges_byOrderNumber
	Scenario: Verify the API Get charges with OrderNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Force
		Then response code should be 201
		When I use HMAC and GET /charges?OrderNumber=IDBRHEIqFd
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		And response body path should contain "orderNumber":"IDBRHEIqFd"
	
	@get-Charges_byTotalAmount
    Scenario: Verify the API Get charges with TotalAmount
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":10.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Force
		Then response code should be 201
		When I use HMAC and GET /charges?total=10
		Then response code should be 200
	 #  And response body path should contain "gatewayId":"@gatewayId"
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		And response body path should contain "total":10
		
	@get-Charges_byStatus
    Scenario Outline: Verify the API Get charges with Status
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":10.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Sale
		Then response code should be 201
		When I use HMAC and GET /charges?status=<Status>
		Then response code should be 200
	 	And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		And response body path should contain "status":"<Status>"
		
		Examples:
		|Status  |
		|Declined|
		|Batch   |
		|Settled |
	#	|Expired |
		
  #	commneted below scenario because of no charges done through Mobile
	@get-Charges_bySource
	Scenario Outline: Verify the API Get charges with Source
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":10.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Sale
		Then response code should be 201
		When I use HMAC and GET /charges?source=<Source>
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "applicationName":"<Value>"

		Examples:
			|Source    | Value |
	  #     | Mobile   | SPS Mobile    |
			|Recurring | SPS Recurring |

	@get-Charges_byAccountNumber
	Scenario Outline: Verify the API Get charges with AccountNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":10.0},"CardData":{"Expiration":"1122","Number":"<cardNumber>", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Sale
		Then response code should be 201
		When I use HMAC and GET /charges?accountNumber=<number>
		Then response code should be 200
		And response header Content-Type should be application/json
		And response body path should contain "accountNumber":"<accountNumber>"
		Examples:
			|cardNumber          | number | accountNumber |
			|4111111111111111    | 1111   | XXXXXXXXXXXX1111 |
			|5454545454545454    | 5454   | XXXXXXXXXXXX5454 |
			|6011000993026909    | 6909   | XXXXXXXXXXXX6909 |
			|371449635392376     | 2376   | XXXXXXXXXXX2376  |

	@get-Charges_byApprovalCode
	Scenario: Verify the API Get charges with ApprovalCode or Code
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=Force
		Then response code should be 201
		When I use HMAC and GET /charges?approvalCode=213213
		Then response code should be 200
	#  And response body path should contain "gatewayId":"@gatewayId"
		And response header Content-Type should be application/json
		And response body path should contain "service":"Bankcard"
		And response body path should contain "responseCode":"213213"


		# +++++++++++++++++    Negative Scinarios   ++++++++++++++++++++++++++
		
	 @get-Charges_byinValidReference
     Scenario: Verify the API Get charges with inValidReference
		 Given I set clientId header to `clientId`
		 And I set merchantId header to `merchantId`
		 And I set merchantKey header to `merchantKey`
		 And I set Content-Type header to "application/json"
		When I use HMAC and GET /charges?reference=fksjldfdsljfs
        Then response code should be 200
        And response body path should not contain "reference":"fksjldfdsljfs"
		And response body path should not contain "service":"Bankcard"
		And response body path should not contain "reference"
		
	@get-Charges_byFutureDate_AsStartDate
     Scenario: Verify the API Get charges with Future StartDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /charges?StartDate=2030-12-12
        Then response code should be 200
        And response body path should contain "startDate":"2030-12-12"
	#	And response body path should not contain "reference"
	#	And response body path should not contain "service"
		
	@get-Charges_byPastDate_AsEndDate
     Scenario: Verify the API Get Charges with Past EndDate
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /charges?EndDate=2012-12-12
        Then response code should be 200
        And response body path should contain "endDate":"2012-12-12"
	#	And response body path should not contain "reference"
	#	And response body path should not contain "service"
		
	@get-Charges_byinValidApprovalCode
     Scenario: Verify the API Get charges with inValid ApprovalCode
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /charges?approvalCode=Test1234
        Then response code should be 200
        And response body path should not contain "responseCode":"Test1234"
		And response body path should not contain "reference"
		And response body path should not contain "service"
		
	@get-Charges_byinValidOrderNumber
     Scenario: Verify the API Get charges with inValidOrderNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /charges?orderNumber=Test123
        Then response code should be 200
		And response body path should not contain "orderNumber":"Test123"
		And response body path should not contain "reference"
		And response body path should not contain "service"

	@get-Charges_byinValidStatus
     Scenario: Verify the API Get charges with inValidStatus
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /charges?Status=Test123
        Then response code should be 200
        And response body path should not contain "status":"Test123"
	#	And response body path should not contain "reference"
	#	And response body path should not contain "service"
				
	@get-Charges_byinValidSource
     Scenario: Verify the API Get charges with inValidSource
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /charges?Source=Test123
        Then response code should be 200
        And response body path should not contain "source":"Test123"
	#	And response body path should not contain "reference"
	#	And response body path should not contain "service"
	
	@get-Charges_byinValidIsPurchaseCard
     Scenario: Verify the API Get charges with inValidPurchaseCard
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and GET /charges?IsPurchaseCard=Test123
        Then response code should be 200
        And response body path should not contain "source":"Test123"
	#	And response body path should not contain "reference"
	#	And response body path should not contain "service"
		
	
	