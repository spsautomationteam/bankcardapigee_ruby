@post_Credits_Reference
Feature: Post Credits Reference
	As an API consumer
	I want to create Credits requests
	So that I know they can be processed

	@post-Credits-Reference_byValidRefNumber
	Scenario: API post Credits Lineitems with valid Reference Number
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":30.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /credits
		Then response code should be 201
		And response body path status should be Approved
		And response body path message should be APPROVED
		When I use POST Credits Reference using Body {"transactionId": "",     "deviceId": "7894560",     "amount": 20,     "terminalNumber": "" }
		Then response code should be 201  
		And response body path status should be Approved
		And response body path message should be APPROVED
		
#	**************** Negative Scenarios **************************

	@post-Credits-Reference_byInvalidRef
	Scenario: API post Credits Reference with invalid Reference
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and POST Credits Reference using Invalid reference number /credits/Test123
		Then response code should be 400
		And response body path code should be 400000
		And response body path message should be There was a problem with the request. Please see 'detail' for more.
		And response body path detail should be InvalidRequestData : INVALID T_REFERENCE
	