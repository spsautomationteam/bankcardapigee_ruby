@post-Charge-Lineitems
Feature: Post Charges Lineitems
	As an API consumer
	I want to create charge requests
	So that I know they can be processed
	
	@post-Charge-Lineitems_Details_byMasterCardDetails_For_AllTransactions
	Scenario Outline: API Post Charge Lineitems with Lineitems Body and byMasterCardDetails_For_AllTransaTypes
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
		When I use HMAC and POST LineItems using lineItem Body {"masterCard": [{"itemDescription": "MasterCard","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
		Then response code should be 201

	Examples:
	|	TypeOfTransaction	|
	|	Sale				|
	|	Authorization		|
	|	Force				|
  #	|   Credit 				|


	@post-Charge-Lineitems_Details_byVisaCardDetails_For_AllTransaTypes
	Scenario Outline: API Post Charge Lineitems with Lineitems Body and byVisaCardDetails_For_AllTransaTypes
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111", "cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /charges?type=<TypeOfTransaction>
		Then response code should be 201
		When I use HMAC and POST LineItems using lineItem Body {"visa": [{"commodityCode": "2","itemDescription": "dfgdfg","productCode": "5","quantity": 4,"unitOfMeasure": "0","unitCost": 40,"vatTaxAmount": 4,"vatTaxRate": 1,"discountAmount": 5,"lineItemTotal": 35}]}
		Then response code should be 201

		Examples:
			|	TypeOfTransaction	|
			|	Sale				|
			|	Authorization		|
			|	Force				|
		  #	|   Credit 				|
		
	
  ################  Negative Scenarios  ################

	@post-Charge-Lineitems_MCard_byInValidRefNumber
	Scenario: API post_charge_lineitems with invalid RefNumber
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and POST LineItems using Invalid reference number /charges/Test123/lineitems
		Then response code should be 404
		And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.

	